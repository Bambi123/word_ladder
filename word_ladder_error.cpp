/*
Thomas French
Date Due: <due date>

The following program will maintain all of the error checking
from word_ladder.cpp
*/
#include "word_ladder.hpp"

// Implemented Function
namespace word_ladder {
	/*
	The following function will perform error checks on the two words given to
	function: generate and will determine two things:
	 - Do the length of the two words match?
	 - Do both words exist in the lexicon dictionary?
	*/
	auto error_check_words(std::string const& word,
	                       std::string const& target,
	                       std::unordered_set<std::string> const& vocab) -> bool {
		// Check for different length words
		if (word.length() != target.length()) {
			return false;
		}

		// Checking if starting word is in the vocabulary
		if (!vocab.count(word)) {
			return false;
		}

		// Checking if the target word is in the vocabulary
		if (!vocab.count(target)) {
			return false;
		}

		// Otherwise, return true
		return true;
	}

} // namespace word_ladder
