/*
Thomas French
Date Due: <due date>

The following program will retrieve a vocabulary of english words and will attempt to
Design a word ladder from a starting word to a target word.

Each progressive step can only change one letter in the word and each new word
has to be a word that currently exists in the vocabulary.
*/
#include "word_ladder.hpp"

// Included Libraries
#include <unordered_set>
#include <vector>

// Implemented Function
namespace word_ladder {
	auto generate(std::string const& word,
	              std::string const& target,
	              std::unordered_set<std::string> const& vocab)
	   -> std::vector<std::vector<std::string>> {
		// Check for all possible errors from words
		if (!error_check_words(word, target, vocab)) {
			return {};
		}

		// If word == target, return appropriate vector
		if (word.compare(target) == 0) {
			return {{word}};
		}

		// Filter out all potential words into filter set
		auto filter = std::unordered_set<std::string>{};
		filter_vocab(filter, vocab, word.length());

		// Build the word ladder into pathways
		auto pathways = std::vector<std::vector<std::string>>{};
		build_pathways(word, target, filter, pathways);

		return pathways;
	}

} // namespace word_ladder
