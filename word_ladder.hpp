#include <unordered_set>
#include <string>
#include <vector>

namespace word_ladder {
	[[nodiscard]] auto read_lexicon(std::string const& path) -> std::unordered_set<std::string>;

	// Given a start word and destination word, returns all the shortest possible paths from the
	// start word to the destination, where each word in an individual path is a valid word per the
	// provided lexicon. Pre: ranges::size(from) == ranges::size(to) Pre: valid_words.contains(from)
	// and valid_words.contains(to)
	[[nodiscard]] auto generate(std::string const& from,
	                            std::string const& to,
	                            std::unordered_set<std::string> const& lexicon)
	   -> std::vector<std::vector<std::string>>;

// MY HELPER FUNCTIONS

	auto error_check_words(std::string const& word,
						   std::string const& target,
						   std::unordered_set<std::string> const& vocab)
						   -> bool;

	auto filter_vocab(std::unordered_set<std::string>& filter,
					  std::unordered_set<std::string> const& vocab,
					  size_t const& word)
					  -> void;

	auto build_pathways(std::string const& word,
                        std::string const& target,
                        std::unordered_set<std::string>& vocab,
                        std::vector<std::vector<std::string>>& pathways)
                        -> void;

    auto connection(std::string const& word1, std::string const& word2) -> bool;

// END HELPER FUNCTIONS

} // namespace word_ladder

