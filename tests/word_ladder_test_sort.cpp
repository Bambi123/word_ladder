/*
Thomas French
Date Due: 18th of June, 2021

The following tests are to specifically check that the returned word ladder
is completely sorted alphabetically as specified for the problem.
These tests cover 5 different scenarios that will carefully check the order
of the output and ensure that they are alphabetical!
*/
#include "comp6771/word_ladder.hpp"

#include <string>
#include <vector>

#include "catch2/catch.hpp"

TEST_CASE("Sorting Scenarios") {
	SECTION("Sort Scenario: abba -> abba") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("abba", "abba", english_lexicon);

		REQUIRE(std::size(ladders) == 1);
		auto elem1 = *ladders.begin();
		CHECK(elem1 == std::vector<std::string>{"abba"});
	}

	SECTION("Sort Scenario: work -> porn") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "porn", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		auto elem1 = *ladders.begin();
		auto elem2 = *ladders.rbegin();
		CHECK(elem1 == std::vector<std::string>{"work", "pork", "porn"});
		CHECK(elem2 == std::vector<std::string>{"work", "worn", "porn"});
	}

	SECTION("Sort Scenario: work -> foam") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "foam", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		auto elem1 = *ladders.begin();
		auto elem2 = *ladders.rbegin();
		CHECK(elem1 == std::vector<std::string>{"work", "fork", "form", "foam"});
		CHECK(elem2 == std::vector<std::string>{"work", "worm", "form", "foam"});
	}

	SECTION("Sort Scenario: give -> have") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("give", "have", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		auto elem1 = *ladders.begin();
		auto elem2 = *ladders.rbegin();
		CHECK(elem1 == std::vector<std::string>{"give", "gave", "have"});
		CHECK(elem2 == std::vector<std::string>{"give", "hive", "have"});
	}

	SECTION("work -> pert") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "pert", english_lexicon);

		REQUIRE(std::size(ladders) == 4);
		auto it = ladders.begin();
		auto elem1 = *it++;
		auto elem2 = *it++;
		auto elem3 = *it++;
		auto elem4 = *it++;
		CHECK(elem1 == std::vector<std::string>{"work", "pork", "perk", "pert"});
		CHECK(elem2 == std::vector<std::string>{"work", "pork", "port", "pert"});
		CHECK(elem3 == std::vector<std::string>{"work", "wort", "port", "pert"});
		CHECK(elem4 == std::vector<std::string>{"work", "wort", "wert", "pert"});
	}
}
