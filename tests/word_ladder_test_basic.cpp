/*
Thomas French
Date Due: 18th of June, 2021

The following tests are based on very basic scenarios for the
word ladder problem.  Where the number of paths is either
 - zero
 - one
 - two
 - or more than two (with a simple ladder)

These tests were chosen as such because they are a quick way
to check if the word ladder is working as it should without
checking for sorting!
*/
#include "comp6771/word_ladder.hpp"

#include <cstdio>
#include <string>
#include <vector>

#include "catch2/catch.hpp"

TEST_CASE("Basic Scenario: Zero Paths") {
	SECTION("airplane -> tricycle") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("airplane", "tricycle", english_lexicon);

		REQUIRE(std::size(ladders) == 0);
	}
}

TEST_CASE("Basic Scenario: One Path") {
	SECTION("at -> it") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("at", "it", english_lexicon);

		REQUIRE(std::size(ladders) == 1);
		CHECK(std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"at", "it"}) == 1);
	}

	SECTION("fly -> sky") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("fly", "sky", english_lexicon);

		REQUIRE(std::size(ladders) == 1);
		CHECK(std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"fly", "sly", "sky"})
		      == 1);
	}

	SECTION("work -> wert") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "wert", english_lexicon);

		REQUIRE(std::size(ladders) == 1);
		CHECK(
		   std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"work", "wort", "wert"})
		   == 1);
	}
}

TEST_CASE("Basic Scenario: Two Paths!") {
	SECTION("work -> porn") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "porn", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		CHECK(
		   std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"work", "worn", "porn"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"work", "pork", "porn"})
		   == 1);
	}

	SECTION("work -> foam") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "foam", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "worm", "form", "foam"})
		      == 1);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "fork", "form", "foam"})
		      == 1);
	}

	SECTION("give -> have") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("give", "have", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		CHECK(
		   std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"give", "hive", "have"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"give", "gave", "have"})
		   == 1);
	}
}

TEST_CASE("Basic Scenario: Many Paths!") {
	SECTION("work -> pert") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "pert", english_lexicon);

		REQUIRE(std::size(ladders) == 4);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "wort", "port", "pert"})
		      == 1);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "wort", "wert", "pert"})
		      == 1);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "pork", "perk", "pert"})
		      == 1);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"work", "pork", "port", "pert"})
		      == 1);
	}

	SECTION("sax -> vig") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("sax", "vig", english_lexicon);

		REQUIRE(std::size(ladders) == 24);
	}
}
