/*
Thomas French
Date Due: 18th of June, 2021

The following test scenarios are very difficult problems for the
word ladder.  These tests are used to test the time performance of
the solution and ensure that they problem still functions correctly
at high effort problems.
*/
#include "comp6771/word_ladder.hpp"

#include <algorithm>
#include <string>
#include <vector>

#include "catch2/catch.hpp"

TEST_CASE("Performance Scenarios") {
	SECTION("awake -> sleep") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("awake", "sleep", english_lexicon);

		REQUIRE(std::size(ladders) == 2);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"awake",
		                                          "aware",
		                                          "sware",
		                                          "share",
		                                          "sharn",
		                                          "shawn",
		                                          "shewn",
		                                          "sheen",
		                                          "sheep",
		                                          "sleep"})
		      == 1);
		CHECK(std::count(ladders.begin(),
		                 ladders.end(),
		                 std::vector<std::string>{"awake",
		                                          "aware",
		                                          "sware",
		                                          "share",
		                                          "shire",
		                                          "shirr",
		                                          "shier",
		                                          "sheer",
		                                          "sheep",
		                                          "sleep"})
		      == 1);
	}

	SECTION("work -> play") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("work", "play", english_lexicon);

		REQUIRE(std::size(ladders) == 12);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "fork", "form", "foam", "flam", "flay", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "pork", "perk", "peak", "pean", "plan", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "pork", "perk", "peak", "peat", "plat", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "pork", "perk", "pert", "peat", "plat", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "pork", "porn", "pirn", "pian", "plan", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "pork", "port", "pert", "peat", "plat", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "word", "wood", "pood", "plod", "ploy", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "worm", "form", "foam", "flam", "flay", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "worn", "porn", "pirn", "pian", "plan", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "wort", "bort", "born", "blat", "plat", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "wort", "port", "pert", "peat", "plat", "play"})
		   == 1);
		CHECK(
		   std::count(ladders.begin(),
		              ladders.end(),
		              std::vector<std::string>{"work", "wort", "wert", "pert", "peat", "plat", "play"})
		   == 1);
	}
}
