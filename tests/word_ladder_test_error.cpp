/*
Thomas French
Date Due: 18th of June, 2021

These tests replicate scenarios wheras the input given to the function
cannot possibly result in a word ladder (not necessarily an error).
These include:
 - If the starting word is not in the given lexicon
 - If the ending word is not in the given lexicon
 - If the length of the starting word and the ending word do not match
 - If the ending word is equal to the starting word
*/
#include "comp6771/word_ladder.hpp"

#include <string>
#include <vector>

#include "catch2/catch.hpp"

TEST_CASE("Error Scenarios!") {
	SECTION("Starting Word is not in Lexicon!") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("abca", "abba", english_lexicon);

		REQUIRE(std::size(ladders) == 0);
	}

	SECTION("Target Word is not in Lexicon!") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("abba", "abca", english_lexicon);

		REQUIRE(std::size(ladders) == 0);
	}

	SECTION("Length of Staring Word and Target Word do not match!") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("aa", "aah", english_lexicon);

		REQUIRE(std::size(ladders) == 0);
	}

	SECTION("Target Word is equal to Starting Word!") {
		auto const english_lexicon = word_ladder::read_lexicon("./test/word_ladder/english.txt");
		auto const ladders = word_ladder::generate("aah", "aah", english_lexicon);

		REQUIRE(std::size(ladders) == 1);
		CHECK(std::count(ladders.begin(), ladders.end(), std::vector<std::string>{"aah"}) == 1);
	}
}
