/*
Thomas French
Date Due: 18th of June, 2021

A helper file to break down the functional implementation of word_ladder.cpp
*/
#include "word_ladder.hpp"

// Included Libraries
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <queue>
#include <vector>

// Implemented Function
namespace word_ladder {
	/*
	The following function build_pathways is the main implementation for the
	assignment.  It will take a set of potential words and map all potential
	pathways from the starting word to the target word.
	*/
	auto build_pathways(std::string const& word,
	                    std::string const& target,
	                    std::unordered_set<std::string>& vocab,
	                    std::vector<std::vector<std::string>>& pathways) -> void {
		// Develop the queue
		auto build_queue = std::queue<std::vector<std::string>>{};
		build_queue.push({word});

		// Track the length of the solution
		auto sol_length = -1;

		// Implement the queue
		while (!build_queue.empty()) {
			// Get the vector from the queue
			auto path = build_queue.front();
			// Get the final element from the path
			auto final_elem = *path.rbegin();
			// Cycle through all connections and put them into a vector
			// Then we can sort the ones to put back into the queue first

			//This has been checked against sorting the list of potential words
			//this method is faster since we sort less words total
			auto inter = std::vector<std::string>{};
			for (auto s : vocab) {
				if (connection(s, final_elem)) {
					auto check = std::find(path.begin(), path.end(), s);
					if (check != path.end()) {
						continue;
					}
					inter.push_back(s);
				}
			}

			// Sort the inter vector alphabetically
			std::sort(inter.begin(), inter.end(), std::less<std::string>());

			// Cycle through all the word with a connection and
			// add them to queue
			for (auto s : inter) {
				// Add the element to the back of path
				path.push_back(s);
				auto path_length = static_cast<int>(path.size());

				// If this word is the final word, add it to pathways
				if (target.compare(s) == 0) {
					// if this is the first instance
					if (sol_length == -1) {
						sol_length = path_length;
					}
					// If sol_length = path_length
					if (sol_length == path_length) {
						pathways.emplace(pathways.end(), path);
					}
				}
				else if (path_length < sol_length || sol_length == -1) {
					build_queue.push(path);
				}
				path.pop_back();
			}

			// Pop off the front value and continue;
			build_queue.pop();
		}
		return;
	}

	/*
	The following function will determine whether or not a connection exists between
	two strings in a BFS.  Performs the following checks:
	 - if two words are equal -> return false
	 - if two words have a connection -> return true
	 - if two words do not have a connection -> return false
	Based on previous functionality, it is given that the two strings are of the
	same length and are both in the lexicon dictionary.
	*/
	auto connection(std::string const& word1, std::string const& word2) -> bool {
		if (word1.compare(word2) == 0) {
			return false;
		} // cannot be connected to itself

		// If a word is only different by one character, then it is connected
		// We must have a no_diff value of one
		auto no_diff = 0;
		for (auto i = static_cast<size_t>(0); i < word1.length() && no_diff < 2; i++) {
			if (word1[i] != word2[i]) {
				no_diff++;
			}
			if (no_diff > 1) {
				return false;
			}
		}
		if (no_diff == 1) {
			return true;
		}
		return false;
	}
	/*
	The following function filter_vocab will take all of the words from the english
	lexicon and put all of the words with the appropriate length into a separate
	unordered_set called filter, filter will then be manipulated in word_ladder.cpp
	*/
	auto filter_vocab(std::unordered_set<std::string>& filter,
	                  std::unordered_set<std::string> const& vocab,
	                  size_t const& length) -> void {
		// Loop through and add any words that are the same length
		for (auto s : vocab) {
			if (s.length() == length) {
				filter.emplace(s);
			}
		}
	}

} // namespace word_ladder
